<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160923064803 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->createTable('list');
        $id = $table->addColumn('id', 'integer', ['unsigned' => true]);
        $table->setPrimaryKey(['id']);
        $id->setAutoincrement(true);
        $parent_id = $table->addColumn('parent_id', 'integer', ['unsigned' => true]);
        $parent_id->setNotnull(false);
        $table->addIndex(['parent_id']);
        $parent_id = $table->addForeignKeyConstraint('list', ['parent_id'], ['id']);

        $title = $table->addColumn('title', 'string');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('list');

    }
}
