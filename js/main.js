$(document).ready(function () {
    $('#add_list').click(function () {
        $(this).addClass('hidden');
        $('#back_to_list').removeClass('hidden');
        $('#lists').addClass('hidden');
        $('#create_list_form').removeClass('hidden');
    });

    $('#back_to_list').click(function () {
        if ($(this).data('href') != '') {
            window.location.href = $(this).data('href');
        }
        else {
            $(this).addClass('hidden');
            $('#add_list').removeClass('hidden');
            $('#lists').removeClass('hidden');
            $('#create_list_form').addClass('hidden');
        }
    });

    $('#confirm_form_button').click(function () {
        $('#add_new_list').submit();
    });

    $(document).on('click', '#list_module #lists .list .remove_list', function (e) {
        $(this).find('.glyphicon').addClass('hidden');
        $(this).find('.fa-spinner').removeClass('hidden');
        if(confirm('Are you sure?')){
            var button = $(this);
            $.ajax({
                url: $(this).data('href'),
                type: 'post',
                success: function (data) {
                    if(data['deleted'] === true){
                        button.closest('.list').remove();
                    }
                }
            });
        }
        e.stopPropagation();
    });

    $("#list_module #lists .list .list_info:not('.create_new_item')").click(function () {
        window.location.href = $(this).data('href');
    });
    $("#list_module #lists .list .list_info.create_new_item").click(function () {
        $(this).closest('.list').find('.list_info').addClass('hidden');
        $(this).closest('.list').find('.edit_list_form').removeClass('hidden');
    });

    $(document).on('click', '#list_module #lists .list .edit_list', function (e) {
        e.stopPropagation();
        $(this).closest('.list').find('.list_info').addClass('hidden');
        $(this).closest('.list').find('.edit_list_form').removeClass('hidden');
    });

    $(document).on('click', '#list_module #lists .list .edit_list_form .discard', function () {
        if($(this).attr('disabled') == 'disabled') {
        }
        else {
            $(this).closest('.list').find('.list_info').removeClass('hidden');
            $(this).closest('.list').find('.edit_list_form').addClass('hidden');
        }
    });

    $(document).on('click', '#list_module #lists .list .edit_list_form .confirm', function () {
        if($(this).attr('disabled') == 'disabled') {
        }
        else {
            var title = ($(this).closest('.edit_list_form').find('input[name="edit_title"]').val());
            var button = $(this);
            $('a').attr('disabled', 'disabled');
            button.find('i.fa-spinner').removeClass('hidden');
            $.ajax({
                url: $(this).data('href'),
                type: 'post',
                data: {list_name: title},
                success: function (data) {
                    button.find('i.fa-spinner').addClass('hidden');
                    button.closest('.list').find('.list_info .title').text(data['title']);
                    button.closest('.list').find('.edit_list_form').addClass('hidden');
                    button.closest('.list').find('.list_info').removeClass('hidden');
                    if (data['create'] === true) {
                        console.log(data['item']);
                        var html = $('#lists .list:first').html();
                        $('#lists .list:last').before("<div class='list new'>" + html + "</div>");
                        $('#lists .list.new .list_info .title').text(data['item']['title']);
                        $('#lists .list.new .edit_list_form input[name="edit_title"]').val(data['item']['title']);

                        var submitHref = $('#lists .list.new .edit_list_form .confirm').data('href');
                        submitHref = submitHref.substring(0, submitHref.lastIndexOf("/") + 1) + data['item']['id'];
                        $('#lists .list.new .edit_list_form .confirm').data('href', submitHref);

                        var deleteHref = $('#lists .list .list-actions .remove_list').data('href');
                        deleteHref = deleteHref.substring(0, deleteHref.lastIndexOf("/") + 1) + data['item']['id'];
                        $('#lists .list.new .list-actions .remove_list').data('href', deleteHref);
                        $('#lists .list.new').removeClass('new');

                    }
                    $('a').removeAttr('disabled');
                }
            });
        }
    });


});