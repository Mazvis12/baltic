<?php

namespace ListBundle\Controller;

use ListBundle\Entity\ListModel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ListsController extends Controller
{
    /**
     * @Route("/", name="listIndex")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $lists = $em->getRepository('ListBundle:ListModel')->findBy(['parentId' => null]);
        $title = 'MY LISTS';
        return $this->render('ListBundle:Lists:index.html.twig', array(
            'lists' => $lists,
            'title' => $title
        ));
    }
    /**
     * @Route("show", defaults={"id" = 0}, name="showList")
     * @Route("show/{id}", name="showList")
     */
    public function showAction($id = 0)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $list = $em->getRepository('ListBundle:ListModel')->find($id);
        if(!is_null($list)) {
            $lists = $list->getChildren();
            $title = strtoupper($list->getTitle());
            $show = true;
            return $this->render('ListBundle:Lists:index.html.twig', array(
                'lists' => $lists,
                'title' => $title,
                'show' => $id
            ));
        }
        return $this->redirectToRoute('listIndex');
    }

    /**
     * @Route("/new", defaults={"id"=null}, name="newList")
     * @Route("/new/{id}", name="newList")
     * @Method({"POST"})
     */
    public function newAction($id = null,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $parent = null;
        if(!is_null($id)){
            $parent = $em->getRepository('ListBundle:ListModel')->find($id  );
        }
        $list = new ListModel();
        $list->setTitle($request->get('list_name'));
        $list->setParent($parent);
        $em->persist($list);
        $em->flush();
        $em->clear();
        if(is_null($id)) {
            return $this->redirect($request->headers->get('referer'));
        }
        else{
            return new JsonResponse(array('create' => true, 'item' => ['title' => $list->getTitle(), 'id' => $list->getId()]));
        }
    }

    /**
     * @Route("edit", defaults={"id" = 0}, name="editList")
     * @Route("edit/{id}", name="editList")
     * @Method({"POST"})
     */
    public function editAction($id = 0, Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $list = $em->getRepository('ListBundle:ListModel')->find($id);
        $list->setTitle($request->get('list_name'));
        $em->persist($list);
        $em->flush();
        $em->clear();
        return new JsonResponse(array('create' => false, 'title' => $list->getTitle()));
    }

    /**
     * @Route("delete", defaults={"id" = 0}, name="deleteList")
     * @Route("delete/{id}", name="deleteList")
     * @Method({"POST"})
     */
    public function deleteAction($id = 0)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $list = $em->getRepository('ListBundle:ListModel')->find($id);
        $children = $list->getChildren();
        foreach ($children as $child){
            $em->remove($child);
        }
        $em->remove($list);
        $em->flush();
        $em->clear();
        return new JsonResponse(array('deleted' => true));
    }

}
